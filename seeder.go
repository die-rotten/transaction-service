package main

import (
	"transaction-service/models"

	"github.com/jinzhu/gorm"
)

func seedStatus(db *gorm.DB) {
	statuses := []models.Status{
		{
			ID:   1,
			Name: "Menunggu Pembayaran",
		},
		{
			ID:   2,
			Name: "Menunggu Konfirmasi",
		},
		{
			ID:   3,
			Name: "Sudah Dibayar",
		},
	}

	for _, status := range statuses {
		var duplicated models.Status

		db.Where("id = ?", status.ID).Find(&duplicated)
		if duplicated.ID != 0 {
			db.Save(&duplicated)
			continue
		}

		db.Create(&status)
	}
}
