package main

import (
	"fmt"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Println("Can't load .env file, no file exist")
	}

	initDB()
	initRedis()
	initAMQP()
	initRepo()
	initService()
	getCommand()
}
