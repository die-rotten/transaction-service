package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
	"transaction-service/repositories"
	"transaction-service/services"

	"github.com/streadway/amqp"
)

// Repository variable
var transactionRepo *repositories.TransactionRepository
var confirmationRepo *repositories.ConfirmationRepository
var approvalRepo *repositories.ApprovalRepository
var itemRepo *repositories.PackageItemRepository
var reportRepo *repositories.ReportRepository

// Service variable
var transactionService *services.TransactionService
var uniqueCodeService *services.UniqueCodeService
var confirmationService *services.ConfirmationService
var reportService *services.ReportService

// AMQP variable
var amqpConn *amqp.Connection

// HTTP client
var client *http.Client

func initAMQP() {
	connString := fmt.Sprintf(
		"amqp://%s:%s@%s:%s/",
		os.Getenv("RABBITMQ_USER"),
		os.Getenv("RABBITMQ_PASSWORD"),
		os.Getenv("RABBITMQ_HOST"),
		os.Getenv("RABBITMQ_PORT"),
	)

	conn, err := amqp.Dial(connString)
	if err != nil {
		log.Fatal(err.Error())
	}

	amqpConn = conn
}

func initRepo() {
	client = &http.Client{
		Timeout: 30 * time.Second,
	}

	transactionRepo = repositories.NewTransactionRepository(db)
	confirmationRepo = repositories.NewConfirmationRepository(db)
	approvalRepo = repositories.NewApprovalRepository(db)
	itemRepo = repositories.NewPackageItemRepository(client, os.Getenv("CONTENT_SERVICE_URL"))
	reportRepo = repositories.NewReportRepository(db)
}

func initService() {
	uniqueCodeService = services.NewUniqueCodeService(redisClient)
	transactionService = services.NewTransactionService(transactionRepo, itemRepo, uniqueCodeService)
	confirmationService = services.NewConfirmationService(
		confirmationRepo,
		approvalRepo,
		uniqueCodeService,
		amqpConn,
	)
	reportService = services.NewReportService(reportRepo)
}
