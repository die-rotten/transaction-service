package repositories

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"transaction-service/dto"
)

// PackageItemRepository :nodoc
type PackageItemRepository struct {
	client  *http.Client
	baseURL string
}

type packageItemResponse struct {
	Data dto.Item `json:"data"`
}

type packageItemListResponse struct {
	Data []dto.Item `json:"data"`
}

// NewPackageItemRepository returns new instance of PackageItemRepository.
func NewPackageItemRepository(client *http.Client, baseURL string) *PackageItemRepository {
	return &PackageItemRepository{
		client:  client,
		baseURL: baseURL,
	}
}

// FetchItem :nodoc
func (repo *PackageItemRepository) FetchItem(id uint) (dto.Item, error) {
	url := fmt.Sprintf("%s/packages/%d", repo.baseURL, id)

	res, err := repo.client.Get(url)
	if err != nil {
		log.Print(err)
		return dto.Item{}, err
	}
	defer res.Body.Close()

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Print(err)
		return dto.Item{}, err
	}

	var itemResponse packageItemResponse
	json.Unmarshal(data, &itemResponse)

	item := dto.Item{
		ID:          itemResponse.Data.ID,
		Title:       itemResponse.Data.Title,
		Description: itemResponse.Data.Description,
		Photo:       itemResponse.Data.Photo,
		Price:       itemResponse.Data.Price,
	}

	return item, nil
}

// Fetch :nodoc
func (repo *PackageItemRepository) Fetch(ids []uint) ([]dto.Item, error) {
	var textIds []string

	for _, id := range ids {
		text := strconv.Itoa(int(id))
		textIds = append(textIds, text)
	}

	params := strings.Join(textIds, ",")
	url := fmt.Sprintf("%s/packages?packageIds=%s", repo.baseURL, params)

	res, err := repo.client.Get(url)
	if err != nil {
		log.Print(err)
		return []dto.Item{}, err
	}
	defer res.Body.Close()

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Print(err)
		return []dto.Item{}, err
	}

	var itemResponse packageItemListResponse
	json.Unmarshal(data, &itemResponse)

	return itemResponse.Data, nil
}
