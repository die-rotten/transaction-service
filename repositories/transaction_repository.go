package repositories

import (
	"fmt"
	"time"
	"transaction-service/models"
	"transaction-service/models/filters"

	"github.com/jinzhu/gorm"
)

// TransactionRepository :nodoc
type TransactionRepository struct {
	db *gorm.DB
}

// NewTransactionRepository return TransactionRepository instance
func NewTransactionRepository(db *gorm.DB) *TransactionRepository {
	return &TransactionRepository{
		db: db,
	}
}

// Get return all transactions
func (repo *TransactionRepository) Get(filter filters.TransactionFilter) ([]models.Transaction, error) {
	transactions := make([]models.Transaction, 0)
	query := repo.db.
		Preload("Item").
		Preload("Confirmations").
		Preload("Status")

	if filter.StatusID != 0 {
		fmt.Println("here")
		query = query.Where("status_id = ?", filter.StatusID)
	}

	err := query.
		Find(&transactions).
		Error

	if err != nil {
		return []models.Transaction{}, err
	}
	return transactions, nil
}

// Find return a transction by ID
func (repo *TransactionRepository) Find(id uint) (models.Transaction, error) {
	var transaction models.Transaction

	err := repo.db.
		Preload("Item").
		Preload("Confirmations").
		Preload("Status").
		First(&transaction, id).
		Error

	if err != nil {
		return transaction, err
	}
	return transaction, nil
}

// FindByUser find transaction by ID and userID.
func (repo *TransactionRepository) FindByUser(id, userID uint) (models.Transaction, error) {
	var transaction models.Transaction

	err := repo.db.
		Preload("Item").
		Preload("Confirmations").
		Preload("Status").
		Where("user_id = ? AND id = ?", userID, id).
		First(&transaction).
		Error
	if err != nil {
		return models.Transaction{}, err
	}

	return transaction, nil
}

// GetBy return transactions by key and value
func (repo *TransactionRepository) GetBy(key string, value interface{}, filter filters.TransactionFilter) ([]models.Transaction, error) {
	transactions := make([]models.Transaction, 0)

	query := repo.db.
		Preload("Item").
		Preload("Confirmations").
		Preload("Status")

	if filter.StatusID != 0 {
		query = query.Where("status_id = ?", filter.StatusID)
	}

	err := query.
		Where(fmt.Sprintf("%s = ?", key), value).
		Find(&transactions).
		Error
	if err != nil {
		return []models.Transaction{}, err
	}
	return transactions, nil
}

// Create store transaction in DB
func (repo *TransactionRepository) Create(data models.Transaction) (models.Transaction, error) {
	tx := repo.db.Begin()

	transactionNumber, err := repo.generateNumber()
	if err != nil {
		tx.Rollback()
		return models.Transaction{}, err
	}

	data.Number = transactionNumber

	err = tx.Create(&data).Error
	if err != nil {
		tx.Rollback()
		return models.Transaction{}, err
	}

	tx.Commit()
	return repo.Find(data.ID)
}

// GetUnpaid :nodoc
func (repo *TransactionRepository) GetUnpaid(userID uint) ([]models.Transaction, error) {
	transactions := make([]models.Transaction, 0)

	err := repo.db.
		Preload("Item").
		Preload("Confirmations").
		Preload("Status").
		Where("user_id = ? AND is_paid = false", userID).
		Find(&transactions).
		Error
	if err != nil {
		return []models.Transaction{}, err
	}

	return transactions, nil
}

func (repo *TransactionRepository) generateNumber() (string, error) {
	var count int

	err := repo.db.Table("transactions").Count(&count).Error
	if err != nil {
		return "", err
	}

	date := time.Now().Format("20060102")
	number := fmt.Sprintf("TR-%s-%d", date, count+1)
	return number, nil
}
