package repositories

import (
	"transaction-service/models"

	"github.com/jinzhu/gorm"
)

// ApprovalRepository :nodoc
type ApprovalRepository struct {
	db *gorm.DB
}

// NewApprovalRepository :nodoc
func NewApprovalRepository(db *gorm.DB) *ApprovalRepository {
	return &ApprovalRepository{
		db: db,
	}
}

// Approve :nodoc
func (repo *ApprovalRepository) Approve(confirmID uint) error {
	var confirmation models.TransactionConfirmation

	tx := repo.db.Begin()
	err := tx.Preload("Transaction").First(&confirmation, confirmID).Error
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Model(&confirmation).Update("is_approved", true).Error
	if err != nil {
		tx.Rollback()
		return err
	}

	transaction := confirmation.Transaction
	err = tx.Model(&transaction).
		Updates(map[string]interface{}{
			"is_paid":   true,
			"status_id": paidStatusID,
		}).
		Error

	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}
