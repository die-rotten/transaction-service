package repositories

import (
	"transaction-service/dto"
	"transaction-service/models"
	"transaction-service/models/entity"
	"transaction-service/models/filters"
)

var waitForConfirmationStatusID = uint(2)
var paidStatusID = uint(3)

// TransactionRepositoryContract is contract of Transaction repo.
type TransactionRepositoryContract interface {
	Get(filter filters.TransactionFilter) ([]models.Transaction, error)
	Find(id uint) (models.Transaction, error)
	FindByUser(id, userID uint) (models.Transaction, error)
	GetBy(key string, value interface{}, filter filters.TransactionFilter) ([]models.Transaction, error)
	Create(data models.Transaction) (models.Transaction, error)
	GetUnpaid(userID uint) ([]models.Transaction, error)
}

// ConfirmationRepositoryContract is contract of Transaction
// confirmation repo
type ConfirmationRepositoryContract interface {
	Find(id uint) (models.TransactionConfirmation, error)
	Confirm(data models.TransactionConfirmation) (models.TransactionConfirmation, error)
	GetUnapprove() ([]models.TransactionConfirmation, error)
}

// ApprovalRepositoryContract is contract of Approve transaction confirmation repo
type ApprovalRepositoryContract interface {
	Approve(id uint) error
}

// PackageItemRepositoryContract :nodoc
type PackageItemRepositoryContract interface {
	FetchItem(id uint) (dto.Item, error)
	Fetch(ids []uint) ([]dto.Item, error)
}

// ReportRepositoryContract :nodoc
type ReportRepositoryContract interface {
	GetTransactionsQuantity() (uint, error)
	GetTransactionAmountByYear(year uint) (entity.TransactionReportByYear, error)
}
