package repositories

import (
	"transaction-service/models"

	"github.com/jinzhu/gorm"
)

// ConfirmationRepository :nodoc
type ConfirmationRepository struct {
	db *gorm.DB
}

// NewConfirmationRepository :nodoc
func NewConfirmationRepository(db *gorm.DB) *ConfirmationRepository {
	return &ConfirmationRepository{
		db: db,
	}
}

// Find :nodoc
func (repo *ConfirmationRepository) Find(id uint) (models.TransactionConfirmation, error) {
	var confirmation models.TransactionConfirmation

	err := repo.db.
		Preload("Transaction").
		Preload("Transaction.Item").
		First(&confirmation, id).
		Error
	if err != nil {
		return models.TransactionConfirmation{}, err
	}
	return confirmation, nil
}

// GetUnapprove :nodoc
func (repo *ConfirmationRepository) GetUnapprove() ([]models.TransactionConfirmation, error) {
	confirmations := make([]models.TransactionConfirmation, 0)

	err := repo.db.
		Preload("Transaction").
		Where("is_approved = false").
		Find(&confirmations).
		Error
	if err != nil {
		return []models.TransactionConfirmation{}, err
	}

	return confirmations, nil
}

// Confirm :nodoc
func (repo *ConfirmationRepository) Confirm(data models.TransactionConfirmation) (models.TransactionConfirmation, error) {
	tx := repo.db.Begin()

	err := repo.db.Create(&data).Error
	if err != nil {
		tx.Rollback()
		return models.TransactionConfirmation{}, err
	}

	var result models.Transaction
	err = tx.Find(&result, data.TransactionID).Error
	if err != nil {
		tx.Rollback()
		return models.TransactionConfirmation{}, nil
	}

	err = tx.Model(&result).
		Update("status_id", waitForConfirmationStatusID).
		Error
	if err != nil {
		tx.Rollback()
		return models.TransactionConfirmation{}, err
	}

	tx.Commit()
	return repo.Find(data.ID)
}
