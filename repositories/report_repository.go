package repositories

import (
	"transaction-service/constants"
	"transaction-service/models"
	"transaction-service/models/entity"

	"github.com/jinzhu/gorm"
)

// ReportRepository :nodoc
type ReportRepository struct {
	db *gorm.DB
}

// NewReportRepository return new instance of report repository.
func NewReportRepository(db *gorm.DB) *ReportRepository {
	return &ReportRepository{
		db: db,
	}
}

// GetTransactionsQuantity return all transaction quantity reports.
func (repository *ReportRepository) GetTransactionsQuantity() (uint, error) {
	var transactionQuantity uint
	var transaction models.Transaction

	err := repository.db.Model(&transaction).
		Where("is_paid = ?", true).
		Count(&transactionQuantity).
		Error
	if err != nil {
		return 0, err
	}

	return transactionQuantity, nil
}

// GetTransactionAmountByYear return all transactions by year.
func (repository *ReportRepository) GetTransactionAmountByYear(year uint) (entity.TransactionReportByYear, error) {
	query := `SELECT TO_CHAR(conducted_at, 'Month') AS month, SUM(amount) as amount FROM transactions 
		WHERE status_id = ?
		AND EXTRACT(Year FROM conducted_at) = ?
		AND EXTRACT(Month FROM conducted_at) = ?
		AND deleted_at IS NULL
		GROUP BY month
	`

	var months = entity.MonthList
	var report entity.TransactionReportByYear

	for _, month := range months {
		var monthly entity.TransactionMonthlyReport

		repository.db.Raw(
			query,
			constants.AlreadyPaidStatusID,
			year,
			month["key"],
		).Scan(&monthly)

		monthly.Month = month["name"].(string)
		report.Data = append(report.Data, monthly)
	}

	report.Year = year

	return report, nil
}
