package handlers

import (
	"net/http"
	"strconv"
	"transaction-service/handlers/responses"
	"transaction-service/services"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// ReportHandler :nodoc
type ReportHandler struct {
	service services.ReportServiceContract
}

// NewReportHandler return handler of report.
func NewReportHandler(service services.ReportServiceContract) *ReportHandler {
	return &ReportHandler{
		service: service,
	}
}

// GetRoutes returns all routes.
func (handler *ReportHandler) GetRoutes() chi.Router {
	router := chi.NewRouter()

	router.Get("/transactions-quantity", handler.GetTransactionQuantity)
	router.Get("/transactions-amount", handler.GetTransactionAmountByYear)

	return router
}

// GetTransactionQuantity :nodoc
func (handler *ReportHandler) GetTransactionQuantity(writer http.ResponseWriter, request *http.Request) {
	quantity, err := handler.service.GetTransactionQuantity()
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	response := responses.NewTransactionQuantityResponse(quantity)
	err = render.Render(writer, request, response)
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}

// GetTransactionAmountByYear :nodoc
func (handler *ReportHandler) GetTransactionAmountByYear(writer http.ResponseWriter, request *http.Request) {
	query := request.URL.Query()["year"]

	if len(query) == 0 {
		render.Render(writer, request, responses.CreateUnprocessableEntityResponse("filter year is required"))
		return
	}

	parsedQuery, err := strconv.Atoi(query[0])
	if err != nil {
		render.Render(writer, request, responses.CreateBadRequestResponse("filter year is invalid"))
		return
	}

	reports, err := handler.service.GetTransactionAmountByYear(uint(parsedQuery))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	response := responses.NewTransactionAmountByYearReport(reports)
	render.Render(writer, request, response)
}
