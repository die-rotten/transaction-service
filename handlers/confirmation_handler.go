package handlers

import (
	"net/http"
	"strconv"
	"time"
	"transaction-service/handlers/middlewares"
	"transaction-service/handlers/requests"
	"transaction-service/handlers/responses"
	"transaction-service/services"
	"transaction-service/shared"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// ConfirmationHandler :nodoc
type ConfirmationHandler struct {
	confirmationService services.ConfirmationServiceContract
	transactionService  services.TransactionServiceContract
}

// NewConfirmationHandler :nodoc
func NewConfirmationHandler(
	confirmationService services.ConfirmationServiceContract,
	transactionService services.TransactionServiceContract,
) *ConfirmationHandler {
	return &ConfirmationHandler{
		confirmationService: confirmationService,
		transactionService:  transactionService,
	}
}

// GetRoutes :nodoc
func (handler *ConfirmationHandler) GetRoutes() chi.Router {
	router := chi.NewRouter()

	router.Route("/", func(r chi.Router) {
		r.Use(middlewares.Auth)
		r.Get("/{id}", handler.Find)
		r.Get("/unapprove", handler.GetUnapprove)
		r.Patch("/{id}/approve", handler.Approve)
		r.Post("/{transactionId}", handler.Confirm)
	})

	return router
}

// Find :nodoc
func (handler *ConfirmationHandler) Find(writer http.ResponseWriter, request *http.Request) {
	idParam := chi.URLParam(request, "id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	confirmation, err := handler.confirmationService.Find(uint(id))
	if confirmation.ID == 0 {
		render.Render(writer, request, responses.CreateNotFoundResponse("Confirmation not found"))
		return
	}

	err = render.Render(writer, request, responses.CreateItemConfirmationResponse(confirmation))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}

// GetUnapprove :nodoc
func (handler *ConfirmationHandler) GetUnapprove(writer http.ResponseWriter, request *http.Request) {
	confirmations, err := handler.confirmationService.GetUnapprove()
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	err = render.Render(writer, request, responses.CreateListConfirmationResponse(confirmations))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}

// Approve :nodoc
func (handler *ConfirmationHandler) Approve(writer http.ResponseWriter, request *http.Request) {
	idParam := chi.URLParam(request, "id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	confirmation, err := handler.confirmationService.Approve(uint(id), "content_service_queue")
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	err = render.Render(writer, request, responses.CreateItemConfirmationResponse(confirmation))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}

// Confirm :nodoc
func (handler *ConfirmationHandler) Confirm(writer http.ResponseWriter, request *http.Request) {
	transactionIDParam := chi.URLParam(request, "transactionId")
	transactionID, err := strconv.Atoi(transactionIDParam)
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	transaction, err := handler.transactionService.Find(uint(transactionID))
	if transaction.ID == 0 {
		render.Render(writer, request, responses.CreateNotFoundResponse("Transaction not found"))
		return
	}

	var confirmationRequest requests.ConfirmationRequest
	err = render.Bind(request, &confirmationRequest)
	if err != nil {
		render.Render(writer, request, responses.CreateUnprocessableEntityResponse(err.Error()))
		return
	}

	userIDCtx := request.Context().Value(shared.UserID)
	userID := userIDCtx.(int)

	confirmation, err := handler.confirmationService.Confirm(
		confirmationRequest.BankName,
		confirmationRequest.SenderName,
		confirmationRequest.Path,
		confirmationRequest.Amount,
		transaction.ID,
		uint(userID),
		time.Now(),
	)

	err = render.Render(writer, request, responses.CreateItemConfirmationResponse(confirmation))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}
