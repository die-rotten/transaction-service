package middlewares

import (
	"context"
	"net/http"
	"transaction-service/handlers/responses"
	"transaction-service/helpers"
	"transaction-service/shared"

	"github.com/go-chi/render"
)

// Auth checking whether request authenticated or not
func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		authHeader := request.Header.Get("Authorization")
		if authHeader == "" {
			render.Render(writer, request, responses.CreateUnauthorizedResponse("Unauthorized"))
			return
		}

		accessToken := authHeader[7:len(authHeader)]
		userID, err := helpers.GetUserID(accessToken)
		if err != nil {
			render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
			return
		}

		authContext := context.WithValue(request.Context(), shared.UserID, userID)
		next.ServeHTTP(writer, request.WithContext(authContext))
	})
}
