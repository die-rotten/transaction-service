package responses

import (
	"transaction-service/models"

	"github.com/go-chi/render"
)

// CreateItemStatusResponse :nodoc
func CreateItemStatusResponse(data models.Status) render.Renderer {
	return &ItemResponse{
		Data: data,
	}
}
