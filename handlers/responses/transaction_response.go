package responses

import (
	"net/http"
	"time"
	"transaction-service/models"

	"github.com/go-chi/render"
)

type transactionResponse struct {
	ID            uint            `json:"id"`
	Number        string          `json:"number"`
	UserID        uint            `json:"userId"`
	ConductedAt   time.Time       `json:"conductedAt"`
	IsPaid        bool            `json:"isPaid"`
	UniqueCode    uint            `json:"uniqueCode"`
	Amount        float64         `json:"amount"`
	CreatedAt     time.Time       `json:"createdAt"`
	UpdatedAt     time.Time       `json:"updatedAt"`
	Item          render.Renderer `json:"item"`
	Confirmations render.Renderer `json:"confirmations"`
}

type transactionItemResponse struct {
	Data transactionResponse `json:"data"`
}

type transactionListResponse struct {
	Data []transactionResponse `json:"data"`
}

// Render :nodoc
func (t *transactionResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func createTransactionResponse(data models.Transaction) transactionResponse {
	return transactionResponse{
		ID:          data.ID,
		UserID:      data.UserID,
		Number:      data.Number,
		ConductedAt: data.ConductedAt.Local(),
		IsPaid:      data.IsPaid,
		UniqueCode:  data.UniqueCode,
		Amount:      data.Amount,
		CreatedAt:   data.CreatedAt.Local(),
		UpdatedAt:   data.UpdatedAt.Local(),
	}
}

// CreateItemTransactionResponse :nodoc
func CreateItemTransactionResponse(data models.Transaction) render.Renderer {
	return &ItemResponse{
		Data: createTransactionResponse(data),
	}
}

// CreateListTransactionResponse :nodoc
func CreateListTransactionResponse(data []models.Transaction) render.Renderer {
	responses := make([]interface{}, 0)
	for _, item := range data {
		transaction := createTransactionResponse(item)
		responses = append(responses, transaction)
	}
	return &ListResponse{Data: responses}
}
