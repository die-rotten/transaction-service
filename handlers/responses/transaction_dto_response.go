package responses

import (
	"time"
	"transaction-service/dto"

	"github.com/go-chi/render"
)

// TransactionDTOResponse :nodoc
type TransactionDTOResponse struct {
	ID            uint            `json:"id"`
	Number        string          `json:"number"`
	ConductedAt   time.Time       `json:"conductedAt"`
	UniqueCode    uint            `json:"uniqueCode"`
	UserID        uint            `json:"userId"`
	IsPaid        bool            `json:"isPaid"`
	Amount        float64         `json:"amount"`
	CreatedAt     time.Time       `json:"createdAt"`
	UpdatedAt     time.Time       `json:"updatedAt"`
	Item          render.Renderer `json:"item"`
	Status        render.Renderer `json:"status"`
	Confirmations render.Renderer `json:"confirmations"`
}

// CreateItemTransactionDTOResponse :nodoc
func CreateItemTransactionDTOResponse(data dto.Transaction) render.Renderer {
	return &ItemResponse{
		Data: TransactionDTOResponse{
			ID:            data.ID,
			Number:        data.Number,
			ConductedAt:   data.ConductedAt.Local(),
			UserID:        data.UserID,
			UniqueCode:    data.UniqueCode,
			IsPaid:        data.IsPaid,
			Amount:        data.Amount,
			CreatedAt:     data.CreatedAt.Local(),
			UpdatedAt:     data.UpdatedAt.Local(),
			Status:        CreateItemStatusResponse(data.Status),
			Item:          CreateItemDTOResponse(data.Item),
			Confirmations: CreateListConfirmationResponse(data.Confirmations),
		},
	}
}

// CreateListTransactionDTOResponse :nodoc
func CreateListTransactionDTOResponse(data []dto.Transaction) render.Renderer {
	responses := make([]interface{}, 0)
	for _, item := range data {
		response := TransactionDTOResponse{
			ID:            item.ID,
			Number:        item.Number,
			ConductedAt:   item.ConductedAt.Local(),
			UniqueCode:    item.UniqueCode,
			UserID:        item.UserID,
			IsPaid:        item.IsPaid,
			Amount:        item.Amount,
			CreatedAt:     item.CreatedAt.Local(),
			UpdatedAt:     item.UpdatedAt.Local(),
			Item:          CreateItemDTOResponse(item.Item),
			Status:        CreateItemStatusResponse(item.Status),
			Confirmations: CreateListConfirmationResponse(item.Confirmations),
		}

		responses = append(responses, response)
	}

	return &ListResponse{
		Data: responses,
	}
}
