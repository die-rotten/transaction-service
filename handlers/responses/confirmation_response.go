package responses

import (
	"net/http"
	"time"
	"transaction-service/models"

	"github.com/go-chi/render"
)

type confirmationResponse struct {
	ID          uint      `json:"id"`
	UserID      uint      `json:"userId"`
	IsApproved  bool      `json:"isApproved"`
	BankName    string    `json:"bankName"`
	SenderName  string    `json:"senderName"`
	Amount      float64   `json:"amount"`
	FilePath    string    `json:"path"`
	ConductedAt time.Time `json:"conductedAt"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedAt"`
}

type confirmationItemResponse struct {
	Data confirmationResponse `json:"data"`
}

type confirmationListResponse struct {
	Data []confirmationResponse `json:"data"`
}

// Render :nodoc
func (t *confirmationResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func createConfirmationResponse(data models.TransactionConfirmation) confirmationResponse {
	return confirmationResponse{
		ID:          data.ID,
		UserID:      data.UserID,
		BankName:    data.BankName,
		SenderName:  data.SenderName,
		Amount:      data.Amount,
		FilePath:    data.FilePath,
		IsApproved:  data.IsApproved,
		ConductedAt: data.ConductedAt.Local(),
		CreatedAt:   data.CreatedAt.Local(),
		UpdatedAt:   data.UpdatedAt.Local(),
	}
}

// CreateItemConfirmationResponse :nodoc
func CreateItemConfirmationResponse(data models.TransactionConfirmation) render.Renderer {
	return &ItemResponse{
		Data: createConfirmationResponse(data),
	}
}

// CreateListConfirmationResponse :nodoc
func CreateListConfirmationResponse(data []models.TransactionConfirmation) render.Renderer {
	responses := make([]interface{}, 0)
	for _, item := range data {
		confirmation := createConfirmationResponse(item)
		responses = append(responses, confirmation)
	}

	return &ListResponse{Data: responses}
}
