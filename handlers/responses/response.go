package responses

import "net/http"

// ItemResponse :nodoc
type ItemResponse struct {
	Data interface{} `json:"data"`
}

// ListResponse :nodoc
type ListResponse struct {
	Data []interface{} `json:"data"`
}

// Render :nodoc
func (i *ItemResponse) Render(writer http.ResponseWriter, request *http.Request) error {
	return nil
}

// Render :nodoc
func (l *ListResponse) Render(writer http.ResponseWriter, request *http.Request) error {
	return nil
}
