package responses

import (
	"transaction-service/dto"

	"github.com/go-chi/render"
)

// ItemDTOResponse :nodoc
type ItemDTOResponse struct {
	ID          uint    `json:"id"`
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Photo       *string `json:"photo"`
	Price       float64 `json:"price"`
}

// CreateItemDTOResponse :nodoc
func CreateItemDTOResponse(data dto.Item) render.Renderer {
	return &ItemResponse{
		Data: ItemDTOResponse{
			ID:          data.ID,
			Title:       data.Title,
			Description: data.Description,
			Photo:       data.Photo,
			Price:       data.Price,
		},
	}
}
