package responses

import (
	"transaction-service/models/entity"

	"github.com/go-chi/render"
)

// TransactionQuantityReportResponse :nodoc
type TransactionQuantityReportResponse struct {
	Amount uint `json:"amount"`
}

// NewTransactionQuantityResponse :nodoc
func NewTransactionQuantityResponse(amount uint) render.Renderer {
	return &ItemResponse{
		Data: TransactionQuantityReportResponse{
			Amount: amount,
		},
	}
}

// NewTransactionAmountByYearReport :nodoc
func NewTransactionAmountByYearReport(report entity.TransactionReportByYear) render.Renderer {
	return &ItemResponse{
		Data: report,
	}
}
