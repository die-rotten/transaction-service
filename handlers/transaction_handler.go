package handlers

import (
	"net/http"
	"strconv"
	"time"
	"transaction-service/handlers/middlewares"
	"transaction-service/handlers/requests"
	"transaction-service/handlers/responses"
	"transaction-service/models/filters"
	"transaction-service/services"
	"transaction-service/shared"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// TransactionHandler represent handler of Transaction.
type TransactionHandler struct {
	service services.TransactionServiceContract
}

// NewTransactionHandler return instance of transaction handler.
func NewTransactionHandler(
	transactionService services.TransactionServiceContract,
) *TransactionHandler {
	return &TransactionHandler{
		service: transactionService,
	}
}

// GetRoutes return all routes for Transaction.
func (handler *TransactionHandler) GetRoutes() chi.Router {
	route := chi.NewRouter()

	route.Get("/", handler.Get)
	route.Get("/{id}", handler.Find)

	route.Route("/by-user", func(r chi.Router) {
		r.Use(middlewares.Auth)
		r.Get("/", handler.GetByUser)
		r.Get("/{id}", handler.FindByUser)
		r.Get("/unpaid", handler.GetUnpaid)
	})

	route.Route("/", func(r chi.Router) {
		r.Use(middlewares.Auth)
		r.Post("/", handler.Create)
	})

	return route
}

// Get returns all transaction
func (handler *TransactionHandler) Get(writer http.ResponseWriter, request *http.Request) {
	filter := handler.handleFilter(request)
	transactions, err := handler.service.Get(filter)
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	err = render.Render(writer, request, responses.CreateListTransactionDTOResponse(transactions))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}

// Find :nodoc
func (handler *TransactionHandler) Find(writer http.ResponseWriter, request *http.Request) {
	idParam := chi.URLParam(request, "id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	transaction, err := handler.service.Find(uint(id))
	if transaction.ID == 0 {
		render.Render(writer, request, responses.CreateNotFoundResponse("Transaction not found"))
		return
	}

	err = render.Render(writer, request, responses.CreateItemTransactionDTOResponse(transaction))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}

// FindByUser :nodoc
func (handler *TransactionHandler) FindByUser(writer http.ResponseWriter, request *http.Request) {
	idParam := chi.URLParam(request, "id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	userID := request.Context().Value(shared.UserID).(int)

	transaction, err := handler.service.FindByUser(uint(id), uint(userID))
	if transaction.ID == 0 {
		render.Render(writer, request, responses.CreateNotFoundResponse("Transaction not found"))
		return
	}

	err = render.Render(writer, request, responses.CreateItemTransactionDTOResponse(transaction))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}

// GetByUser :nodoc
func (handler *TransactionHandler) GetByUser(writer http.ResponseWriter, request *http.Request) {
	userIDctx := request.Context().Value(shared.UserID)
	userID := userIDctx.(int)

	filter := handler.handleFilter(request)

	transactions, err := handler.service.GetByUser(uint(userID), filter)
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	err = render.Render(writer, request, responses.CreateListTransactionDTOResponse(transactions))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}

// GetUnpaid :nodoc
func (handler *TransactionHandler) GetUnpaid(writer http.ResponseWriter, request *http.Request) {
	userIDctx := request.Context().Value(shared.UserID)
	userID := userIDctx.(int)

	transactions, err := handler.service.GetUnpaid(uint(userID))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	err = render.Render(writer, request, responses.CreateListTransactionDTOResponse(transactions))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}

// Create :nodoc
func (handler *TransactionHandler) Create(writer http.ResponseWriter, request *http.Request) {
	var transactionRequest requests.TransactionRequest

	err := render.Bind(request, &transactionRequest)
	if err != nil {
		render.Render(writer, request, responses.CreateUnprocessableEntityResponse(err.Error()))
		return
	}

	userIDCtx := request.Context().Value(shared.UserID)
	userID := userIDCtx.(int)
	now := time.Now()

	transaction, err := handler.service.Create(
		uint(userID),
		transactionRequest.PackageID,
		transactionRequest.Amount,
		now,
		now,
		now,
	)
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}

	writer.WriteHeader(http.StatusCreated)
	err = render.Render(writer, request, responses.CreateItemTransactionResponse(transaction))
	if err != nil {
		render.Render(writer, request, responses.CreateInternalServerErrorResponse(err.Error()))
		return
	}
}

func (handler *TransactionHandler) handleFilter(request *http.Request) filters.TransactionFilter {
	var filter filters.TransactionFilter

	queries := request.URL.Query()
	if len(queries) > 0 && len(queries["statusId"]) > 0 {
		castedStatusID, _ := strconv.Atoi(queries["statusId"][0])
		filter.StatusID = uint(castedStatusID)
	}

	return filter
}
