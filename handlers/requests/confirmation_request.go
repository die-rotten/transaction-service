package requests

import (
	"errors"
	"net/http"
)

// ConfirmationRequest :nodoc
type ConfirmationRequest struct {
	BankName   string  `json:"bankName"`
	SenderName string  `json:"senderName"`
	Amount     float64 `json:"amount"`
	Path       string  `json:"path"`
}

// Bind :nodoc
func (req *ConfirmationRequest) Bind(request *http.Request) error {
	if req.BankName == "" {
		return errors.New("Field `bankName` is required")
	}

	if req.SenderName == "" {
		return errors.New("Field `senderName` is required")
	}

	if req.Amount == 0 {
		return errors.New("Field `amount` is required")
	}

	if req.Path == "" {
		return errors.New("Field `path` is required")
	}

	return nil
}
