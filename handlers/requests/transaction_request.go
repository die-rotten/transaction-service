package requests

import (
	"errors"
	"net/http"
)

// TransactionRequest :nodoc
type TransactionRequest struct {
	PackageID uint    `json:"packageId"`
	Amount    float64 `json:"amount"`
}

// Bind :nodoc
func (req TransactionRequest) Bind(request *http.Request) error {
	if req.PackageID == 0 {
		return errors.New("Field `packageId` is required")
	}

	if req.Amount == 0 {
		return errors.New("Field `amount` is required")
	}
	return nil
}
