package constants

// WaitForPaymentStatusID :nodoc
var WaitForPaymentStatusID = 1

// WaitForConfirmationStatusID :nodoc
var WaitForConfirmationStatusID = 2

// AlreadyPaidStatusID :nodoc
var AlreadyPaidStatusID = 3
