package helpers

import (
	"strconv"

	"github.com/dgrijalva/jwt-go"
)

// GetUserID extracting token to get UserID
func GetUserID(accessToken string) (int, error) {
	mapClaims := jwt.MapClaims{}

	jwt.ParseWithClaims(accessToken, mapClaims, nil)
	idParam := mapClaims["sub"].(string)

	userID, err := strconv.Atoi(idParam)
	if err != nil {
		return 0, err
	}

	return userID, nil
}
