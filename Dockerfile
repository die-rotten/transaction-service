FROM golang:1.14-alpine AS build

WORKDIR /build
ADD . .

RUN go get -v
RUN CG0_ENABLED=0 go build

FROM alpine

WORKDIR /usr/local/bin

COPY --from=build /build/transaction-service ./transaction-service

RUN chmod +x ./transaction-service

CMD ["transaction-service", "serve"]