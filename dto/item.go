package dto

// Item representing package object from ContentService.
type Item struct {
	ID          uint
	Title       string
	Description string
	Photo       *string
	Price       float64
}
