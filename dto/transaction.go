package dto

import (
	"time"
	"transaction-service/models"
)

// Transaction is representative of Transaction's DTO.
type Transaction struct {
	ID            uint
	Number        string
	ConductedAt   time.Time
	UserID        uint
	IsPaid        bool
	UniqueCode    uint
	Amount        float64
	CreatedAt     time.Time
	UpdatedAt     time.Time
	Item          Item
	Status        models.Status
	Confirmations []models.TransactionConfirmation
}
