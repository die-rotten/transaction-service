package services

import (
	"testing"
	"transaction-service/mocks"
	"transaction-service/models/entity"
)

func TestGetTransactionQuantityReportService(t *testing.T) {
	reportRepoMock := &mocks.ReportRepositoryMock{}
	reportNumber := 5
	reportRepoMock.On("GetTransactionsQuantity").Return(reportNumber, nil)

	reportServiceMock := ReportService{
		reportRepository: reportRepoMock,
	}

	actual, err := reportServiceMock.GetTransactionQuantity()
	if err != nil {
		t.Errorf("GetTransactionQuantity() is fail!")
	}

	if actual != uint(reportNumber) {
		t.Errorf("Result is not expected")
	}
}

func TestGetTransactionAmountByYearReportService(t *testing.T) {
	reportRepoMock := &mocks.ReportRepositoryMock{}

	data := entity.TransactionMonthlyReport{
		Month:  "January",
		Amount: 50000,
	}

	expected := entity.TransactionReportByYear{
		Year: 2020,
		Data: []entity.TransactionMonthlyReport{
			data,
		},
	}

	reportRepoMock.On("GetTransactionAmountByYear", expected.Year).Return(expected, nil)
	reportService := ReportService{
		reportRepository: reportRepoMock,
	}

	actual, err := reportService.GetTransactionAmountByYear(expected.Year)
	if err != nil {
		t.Errorf("GetTransactionAmountByYear() is fail!")
	}

	if actual.Year != expected.Year {
		t.Errorf("Actual and expected data is not equal!")
	}
}
