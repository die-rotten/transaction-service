package services

import (
	"testing"
	"time"
	"transaction-service/dto"
	"transaction-service/mocks"
	"transaction-service/models"
	"transaction-service/models/filters"
)

func TestGetTransactionService(t *testing.T) {
	repo := &mocks.TransactionRepositoryMock{}
	itemRepo := &mocks.PackageItemRepositoryMock{}

	expected := []models.Transaction{
		{
			ID:     1,
			UserID: 1,
			Item: models.TransactionItem{
				PackageID: 1,
			},
		},
	}

	expectedItem := []dto.Item{
		{
			ID: 1,
		},
	}

	filter := filters.TransactionFilter{
		StatusID: 0,
	}

	repo.On("Get", filter).Return(expected, nil)
	itemRepo.On("Fetch", []uint{1}).Return(expectedItem, nil)

	transactionService := TransactionService{
		transactionRepo: repo,
		itemRepo:        itemRepo,
	}

	actual, err := transactionService.Get(filter)
	if err != nil {
		t.Error("Get() on TransactionService is fail!")
	}

	if len(actual) != 1 {
		t.Error("Fail to return expected data")
	}
}

func TestFindTransactionService(t *testing.T) {
	repo := &mocks.TransactionRepositoryMock{}
	itemRepo := &mocks.PackageItemRepositoryMock{}

	expected := models.Transaction{
		ID:         1,
		UserID:     1,
		Number:     "TRX-001",
		UniqueCode: 123,
		Item: models.TransactionItem{
			PackageID: 1,
		},
	}

	expectedDTO := dto.Item{
		ID:          1,
		Title:       "Test",
		Description: "Test",
		Price:       10000,
	}

	repo.On("Find", expected.ID).Return(expected, nil)
	itemRepo.On("FetchItem", expectedDTO.ID).Return(expectedDTO, nil)

	transactionService := TransactionService{
		transactionRepo: repo,
		itemRepo:        itemRepo,
	}

	actual, err := transactionService.Find(expected.ID)
	if err != nil {
		t.Error("TransactionService.Find() is fail!")
	}

	if actual.ID == 0 {
		t.Error("TransactionService.Find() fail to find data")
	}

	if actual.ID != expected.ID {
		t.Error("TransactionService.Find() fail to find expected data")
	}
}

func TestFindByUserTransactionService(t *testing.T) {
	repo := &mocks.TransactionRepositoryMock{}
	itemRepo := &mocks.PackageItemRepositoryMock{}

	expected := models.Transaction{
		ID:         1,
		UserID:     1,
		Number:     "TRX-001",
		UniqueCode: 123,
		Item: models.TransactionItem{
			PackageID: 1,
		},
	}

	expectedDTO := dto.Item{
		ID:          1,
		Title:       "Test",
		Description: "Test",
		Price:       10000,
	}

	repo.On("FindByUser", expected.ID, expected.UserID).Return(expected, nil)
	itemRepo.On("FetchItem", expectedDTO.ID).Return(expectedDTO, nil)

	transactionService := TransactionService{
		transactionRepo: repo,
		itemRepo:        itemRepo,
	}

	actual, err := transactionService.FindByUser(expected.ID, expected.UserID)
	if err != nil {
		t.Error("TransactionService.Find() is fail!")
	}

	if actual.ID == 0 {
		t.Error("TransactionService.Find() fail to find data")
	}

	if actual.ID != expected.ID {
		t.Error("TransactionService.Find() fail to find expected data")
	}
}

func TestGetByUserTransactionService(t *testing.T) {
	repo := &mocks.TransactionRepositoryMock{}
	itemRepo := &mocks.PackageItemRepositoryMock{}

	expected := []models.Transaction{
		{
			ID:     1,
			UserID: 1,
			Item: models.TransactionItem{
				PackageID: 1,
			},
		},
		{
			ID:     2,
			UserID: 1,
			Item: models.TransactionItem{
				PackageID: 2,
			},
		},
	}

	expectedItem := []dto.Item{
		{
			ID: 1,
		},
		{
			ID: 2,
		},
	}

	filter := filters.TransactionFilter{
		StatusID: 0,
	}

	repo.On("GetBy", "user_id", uint(1), filter).Return(expected, nil)
	itemRepo.On("Fetch", []uint{1, 2}).Return(expectedItem, nil)

	transactionService := TransactionService{
		transactionRepo: repo,
		itemRepo:        itemRepo,
	}

	actual, err := transactionService.GetByUser(1, filter)
	if err != nil {
		t.Error("Get() on TransactionService is fail!")
	}

	if len(actual) != 2 {
		t.Error("Fail to return expected data")
	}
}

func TestGetUnpaidTransactionService(t *testing.T) {
	repo := &mocks.TransactionRepositoryMock{}
	itemRepo := &mocks.PackageItemRepositoryMock{}

	expected := []models.Transaction{
		{
			ID:     1,
			UserID: 1,
			Item: models.TransactionItem{
				PackageID: 1,
			},
		},
	}

	expectedItem := []dto.Item{
		{
			ID: 1,
		},
	}

	repo.On("GetUnpaid", uint(1)).Return(expected, nil)
	itemRepo.On("Fetch", []uint{1}).Return(expectedItem, nil)

	transactionService := TransactionService{
		transactionRepo: repo,
		itemRepo:        itemRepo,
	}

	actual, err := transactionService.GetUnpaid(1)
	if err != nil {
		t.Error("Get() on TransactionService is fail!")
	}

	if len(actual) != 1 {
		t.Error("Fail to return expected data")
	}
}

func TestCreateTransactionService(t *testing.T) {
	repo := &mocks.TransactionRepositoryMock{}
	ucService := &mocks.UniqueCodeServiceMock{}
	now := time.Now()

	transaction := models.Transaction{
		UserID:      1,
		ConductedAt: now,
		Amount:      5000,
		UniqueCode:  uint(123),
		IsPaid:      false,
		CreatedAt:   now,
		UpdatedAt:   now,
		StatusID:    1,
		Item: models.TransactionItem{
			Amount:    5000,
			Quantity:  1,
			PackageID: 1,
			CreatedAt: now,
			UpdatedAt: now,
		},
	}

	expected := models.Transaction{
		ID:          1,
		UserID:      1,
		UniqueCode:  123,
		Amount:      5000,
		ConductedAt: now,
		CreatedAt:   now,
		UpdatedAt:   now,
	}

	repo.On("Create", transaction).Return(expected, nil)
	repo.On("Find", expected.ID).Return(expected, nil)
	ucService.On("Generate").Return(123, nil)

	transactionService := TransactionService{
		transactionRepo:   repo,
		uniqueCodeService: ucService,
	}

	actual, err := transactionService.Create(
		1,
		1,
		5000,
		now,
		now,
		now,
	)

	if err != nil {
		t.Error("Create() on TransactionService is fail!")
	}

	if actual.ID != expected.ID {
		t.Error("Create() fail to create expected data")
	}
}
