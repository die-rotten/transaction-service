package services

import (
	"transaction-service/models/entity"
	"transaction-service/repositories"
)

// ReportService :nodoc
type ReportService struct {
	reportRepository repositories.ReportRepositoryContract
}

// NewReportService return new instance of ReportService.
func NewReportService(reportRepository repositories.ReportRepositoryContract) *ReportService {
	return &ReportService{
		reportRepository: reportRepository,
	}
}

// GetTransactionQuantity return quantity of transactions.
func (service *ReportService) GetTransactionQuantity() (uint, error) {
	return service.reportRepository.GetTransactionsQuantity()
}

// GetTransactionAmountByYear return transaction amount by a year.
func (service *ReportService) GetTransactionAmountByYear(year uint) (entity.TransactionReportByYear, error) {
	return service.reportRepository.GetTransactionAmountByYear(year)
}
