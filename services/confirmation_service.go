package services

import (
	"encoding/json"
	"time"
	"transaction-service/models"
	"transaction-service/repositories"

	"github.com/streadway/amqp"
)

// ConfirmationService :nodoc
type ConfirmationService struct {
	confirmationRepo  repositories.ConfirmationRepositoryContract
	approvalRepo      repositories.ApprovalRepositoryContract
	uniqueCodeService UniqueCodeServiceContract
	amqpConn          *amqp.Connection
}

// NewConfirmationService :nodoc
func NewConfirmationService(
	confirmationRepo repositories.ConfirmationRepositoryContract,
	approvalRepo repositories.ApprovalRepositoryContract,
	uniqueCodeService UniqueCodeServiceContract,
	amqpConn *amqp.Connection,
) *ConfirmationService {
	return &ConfirmationService{
		confirmationRepo:  confirmationRepo,
		approvalRepo:      approvalRepo,
		uniqueCodeService: uniqueCodeService,
		amqpConn:          amqpConn,
	}
}

// Find :nodoc
func (service *ConfirmationService) Find(id uint) (models.TransactionConfirmation, error) {
	return service.confirmationRepo.Find(id)
}

// GetUnapprove :nodoc
func (service *ConfirmationService) GetUnapprove() ([]models.TransactionConfirmation, error) {
	return service.confirmationRepo.GetUnapprove()
}

// Confirm :nodoc
func (service *ConfirmationService) Confirm(
	bankName,
	senderName,
	filePath string,
	amount float64,
	transactionID,
	userID uint,
	now time.Time,
) (models.TransactionConfirmation, error) {
	data := models.TransactionConfirmation{
		TransactionID: transactionID,
		UserID:        userID,
		IsApproved:    false,
		BankName:      bankName,
		SenderName:    senderName,
		Amount:        amount,
		FilePath:      filePath,
		ConductedAt:   now,
	}

	confirmation, err := service.confirmationRepo.Confirm(data)
	if err != nil {
		return models.TransactionConfirmation{}, err
	}

	return confirmation, nil
}

// Approve :nodoc
func (service *ConfirmationService) Approve(id uint, queueName string) (models.TransactionConfirmation, error) {
	err := service.approvalRepo.Approve(id)
	if err != nil {
		return models.TransactionConfirmation{}, err
	}

	err = service.uniqueCodeService.ReturnBack()
	if err != nil {
		return models.TransactionConfirmation{}, err
	}

	confirmation, err := service.Find(id)
	if err != nil {
		return models.TransactionConfirmation{}, err
	}

	service.proceedTransactionToQueue(confirmation.Transaction, queueName)
	return confirmation, nil
}

func (service *ConfirmationService) proceedTransactionToQueue(transaction models.Transaction, queueName string) (bool, error) {
	ch, err := service.amqpConn.Channel()
	if err != nil {
		return false, err
	}

	queue, err := ch.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return false, err
	}

	message := map[string]interface{}{
		"user_id":     transaction.UserID,
		"package_ids": []uint{transaction.Item.PackageID},
	}

	marshalledMsg, err := json.Marshal(message)
	if err != nil {
		return false, err
	}

	err = ch.Publish(
		"",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        marshalledMsg,
		},
	)
	return true, nil
}
