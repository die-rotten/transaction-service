package services

import (
	"time"
	"transaction-service/dto"
	"transaction-service/models"
	"transaction-service/models/filters"
	"transaction-service/repositories"
)

var waitForPaymentStatusID = uint(1)

// TransactionService represent service layer of Transaction.
type TransactionService struct {
	transactionRepo   repositories.TransactionRepositoryContract
	itemRepo          repositories.PackageItemRepositoryContract
	uniqueCodeService UniqueCodeServiceContract
}

// NewTransactionService return new instance of TransactionService
func NewTransactionService(
	transactionRepo repositories.TransactionRepositoryContract,
	itemRepo repositories.PackageItemRepositoryContract,
	uniqueCodeService UniqueCodeServiceContract,
) *TransactionService {
	return &TransactionService{
		transactionRepo:   transactionRepo,
		itemRepo:          itemRepo,
		uniqueCodeService: uniqueCodeService,
	}
}

// Get :nodoc
func (service *TransactionService) Get(filter filters.TransactionFilter) ([]dto.Transaction, error) {
	transactions, err := service.transactionRepo.Get(filter)
	if err != nil {
		return []dto.Transaction{}, err
	}

	transactionsDTO, err := service.mapTransactions(transactions)
	if err != nil {
		return []dto.Transaction{}, err
	}

	return transactionsDTO, nil
}

func (service *TransactionService) mapTransactions(transactions []models.Transaction) ([]dto.Transaction, error) {
	packageIds := make([]uint, 0)
	uniqueIDMap := make(map[uint]bool)

	for _, transaction := range transactions {
		if !uniqueIDMap[transaction.Item.PackageID] {
			packageIds = append(packageIds, transaction.Item.PackageID)
			uniqueIDMap[transaction.Item.PackageID] = true
		}
	}

	items, _ := service.itemRepo.Fetch(packageIds)
	itemsMap := make(map[uint]dto.Item)

	for _, item := range items {
		itemsMap[item.ID] = item
	}

	transactionsDTO := make([]dto.Transaction, 0)
	for _, transaction := range transactions {
		item := dto.Transaction{
			ID:            transaction.ID,
			Number:        transaction.Number,
			Amount:        transaction.Amount,
			UserID:        transaction.UserID,
			UniqueCode:    transaction.UniqueCode,
			ConductedAt:   transaction.ConductedAt,
			CreatedAt:     transaction.CreatedAt,
			UpdatedAt:     transaction.UpdatedAt,
			IsPaid:        transaction.IsPaid,
			Status:        transaction.Status,
			Item:          itemsMap[transaction.Item.PackageID],
			Confirmations: transaction.Confirmations,
		}

		transactionsDTO = append(transactionsDTO, item)
	}

	return transactionsDTO, nil
}

// Find :nodoc
func (service *TransactionService) Find(id uint) (dto.Transaction, error) {
	transaction, err := service.transactionRepo.Find(id)
	if err != nil {
		return dto.Transaction{}, err
	}

	item, err := service.itemRepo.FetchItem(transaction.Item.PackageID)
	transactionDTO := dto.Transaction{
		ID:            transaction.ID,
		Number:        transaction.Number,
		Amount:        transaction.Amount,
		UserID:        transaction.UserID,
		UniqueCode:    transaction.UniqueCode,
		ConductedAt:   transaction.ConductedAt,
		CreatedAt:     transaction.CreatedAt,
		UpdatedAt:     transaction.UpdatedAt,
		IsPaid:        transaction.IsPaid,
		Status:        transaction.Status,
		Item:          item,
		Confirmations: transaction.Confirmations,
	}

	return transactionDTO, nil
}

// FindByUser :nodoc
func (service *TransactionService) FindByUser(id, userID uint) (dto.Transaction, error) {
	transaction, err := service.transactionRepo.FindByUser(id, userID)
	if err != nil {
		return dto.Transaction{}, err
	}

	item, err := service.itemRepo.FetchItem(transaction.Item.PackageID)
	transactionDTO := dto.Transaction{
		ID:            transaction.ID,
		Number:        transaction.Number,
		Amount:        transaction.Amount,
		UserID:        transaction.UserID,
		UniqueCode:    transaction.UniqueCode,
		ConductedAt:   transaction.ConductedAt,
		CreatedAt:     transaction.CreatedAt,
		UpdatedAt:     transaction.UpdatedAt,
		IsPaid:        transaction.IsPaid,
		Status:        transaction.Status,
		Item:          item,
		Confirmations: transaction.Confirmations,
	}

	return transactionDTO, nil
}

// GetByUser :nodoc
func (service *TransactionService) GetByUser(userID uint, filter filters.TransactionFilter) ([]dto.Transaction, error) {
	transactions, err := service.transactionRepo.GetBy("user_id", userID, filter)
	if err != nil {
		return []dto.Transaction{}, err
	}

	transactionsDTO, err := service.mapTransactions(transactions)
	if err != nil {
		return []dto.Transaction{}, err
	}

	return transactionsDTO, nil
}

// GetUnpaid :nodoc
func (service *TransactionService) GetUnpaid(userID uint) ([]dto.Transaction, error) {
	transactions, err := service.transactionRepo.GetUnpaid(userID)
	if err != nil {
		return []dto.Transaction{}, err
	}

	transactionsDTO, err := service.mapTransactions(transactions)
	if err != nil {
		return []dto.Transaction{}, err
	}

	return transactionsDTO, nil
}

// Create :nodoc
func (service *TransactionService) Create(
	userID uint,
	packageID uint,
	amount float64,
	conductedAt time.Time,
	createdAt time.Time,
	updatedAt time.Time,
) (models.Transaction, error) {
	uniqueCode, err := service.uniqueCodeService.Generate()
	if err != nil {
		return models.Transaction{}, err
	}

	transaction := models.Transaction{
		UserID:      userID,
		ConductedAt: conductedAt,
		Amount:      amount,
		UniqueCode:  uint(uniqueCode),
		IsPaid:      false,
		CreatedAt:   createdAt,
		UpdatedAt:   updatedAt,
		StatusID:    waitForPaymentStatusID,
		Item: models.TransactionItem{
			Amount:    amount,
			Quantity:  1,
			PackageID: packageID,
			CreatedAt: createdAt,
			UpdatedAt: updatedAt,
		},
	}

	transaction, err = service.transactionRepo.Create(transaction)
	if err != nil {
		return models.Transaction{}, err
	}

	return transaction, nil
}
