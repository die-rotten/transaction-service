package services

import (
	"fmt"
	"os"
	"testing"
	"time"
	"transaction-service/mocks"
	"transaction-service/models"

	"github.com/streadway/amqp"
)

func TestConfirmConfirmationService(t *testing.T) {
	confirmRepo := &mocks.ConfirmationRepositoryMock{}
	now := time.Now()
	data := models.TransactionConfirmation{
		TransactionID: 1,
		UserID:        1,
		Amount:        5000,
		FilePath:      "/files/test.jpg",
		BankName:      "ABC",
		SenderName:    "Yuri",
		ConductedAt:   now,
	}

	expected := models.TransactionConfirmation{
		ID:            1,
		TransactionID: 1,
		UserID:        1,
		Amount:        5000,
		FilePath:      "/files/test.jpg",
		BankName:      "ABC",
		SenderName:    "Yuri",
	}

	confirmRepo.On("Find", expected.ID).Return(expected, nil)
	confirmRepo.On("Confirm", data).Return(expected, nil)

	service := ConfirmationService{
		confirmationRepo: confirmRepo,
	}

	actual, err := service.Confirm(
		"ABC",
		"Yuri",
		"/files/test.jpg",
		5000,
		1,
		1,
		now,
	)

	if err != nil {
		t.Error("Confirm() on ConfirmationService is fail!")
	}

	if actual.ID != expected.ID {
		t.Error("Confirm() on ConfirmationService is fail to create expected data")
	}
}

func TestFindConfirmationService(t *testing.T) {
	confirmRepo := &mocks.ConfirmationRepositoryMock{}
	expected := models.TransactionConfirmation{
		ID:            1,
		TransactionID: 1,
		UserID:        1,
		Amount:        5000,
		FilePath:      "/files/test.jpg",
		BankName:      "ABC",
		SenderName:    "Yuri",
	}

	confirmRepo.On("Find", expected.ID).Return(expected, nil)
	confirmationService := ConfirmationService{
		confirmationRepo: confirmRepo,
	}

	actual, err := confirmationService.Find(expected.ID)
	if err != nil {
		t.Error("Find() on ConfirmationService is fail!")
	}

	if actual.ID != expected.ID {
		t.Error("Find() on ConfirmationService fail to find expected data!")
	}
}

func TestGetUnapproveConfirmationService(t *testing.T) {
	confirmRepo := &mocks.ConfirmationRepositoryMock{}
	expected := []models.TransactionConfirmation{
		{
			ID:            1,
			TransactionID: 1,
			UserID:        1,
			Amount:        5000,
			FilePath:      "/files/test.jpg",
			BankName:      "ABC",
			SenderName:    "Yuri",
			IsApproved:    false,
		},
	}

	confirmRepo.On("GetUnapprove").Return(expected, nil)
	confirmationService := ConfirmationService{
		confirmationRepo: confirmRepo,
	}

	actual, err := confirmationService.GetUnapprove()
	if err != nil {
		t.Error("GetUnapprove() on ConfirmationService is fail!")
	}

	if len(actual) != 1 {
		t.Error("GetUnapprove() on ConfirmationService fail to find expected data!")
	}
}

func TestApproveConfirmationService(t *testing.T) {
	approvalRepo := &mocks.ApprovalRepositoryMock{}
	confirmationRepo := &mocks.ConfirmationRepositoryMock{}
	uniqueCodeService := &mocks.UniqueCodeServiceMock{}

	expected := models.TransactionConfirmation{
		ID:            1,
		TransactionID: 1,
		UserID:        1,
		Amount:        5000,
		IsApproved:    true,
		FilePath:      "/files/test.jpg",
		BankName:      "ABC",
		SenderName:    "Yuri",
		Transaction: models.Transaction{
			IsPaid: true,
		},
	}

	confirmationRepo.On("Find", expected.ID).Return(expected, nil)
	approvalRepo.On("Approve", uint(1)).Return(nil)
	uniqueCodeService.On("ReturnBack").Return(nil)

	conn, err := setupAMQP()
	if err != nil {
		t.Errorf("Fail to setup AMQP()")
	}

	confirmationService := ConfirmationService{
		approvalRepo:      approvalRepo,
		confirmationRepo:  confirmationRepo,
		uniqueCodeService: uniqueCodeService,
		amqpConn:          conn,
	}

	actual, err := confirmationService.Approve(uint(1), "test_content_service_queue")
	if err != nil {
		t.Error("Approve() in ConfirmationService is fail!")
	}

	if actual.IsApproved != true {
		t.Error("Approve() fail to update data")
	}

	if actual.Transaction.IsPaid != true {
		t.Error("Approve() fail to update data")
	}
}

func setupAMQP() (*amqp.Connection, error) {
	connString := fmt.Sprintf(
		"amqp://%s:%s@%s:%s/",
		os.Getenv("RABBITMQ_USER"),
		os.Getenv("RABBITMQ_PASSWORD"),
		os.Getenv("RABBITMQ_HOST"),
		os.Getenv("RABBITMQ_PORT"),
	)

	conn, err := amqp.Dial(connString)
	if err != nil {
		return &amqp.Connection{}, err
	}

	return conn, err
}
