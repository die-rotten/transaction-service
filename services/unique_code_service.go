package services

import (
	"strconv"

	"github.com/go-redis/redis"
)

const (
	activeKey = "available_uc"
	usedKey   = "used_uc"
)

// UniqueCodeService :nodoc
type UniqueCodeService struct {
	redis *redis.Client
}

// NewUniqueCodeService :nodoc
func NewUniqueCodeService(client *redis.Client) *UniqueCodeService {
	return &UniqueCodeService{
		redis: client,
	}
}

// Init initiate key
func (service *UniqueCodeService) Init(start, end uint) {
	for i := start; i <= end; i++ {
		service.redis.RPush(activeKey, i)
	}
}

// Generate :nodoc
func (service *UniqueCodeService) Generate() (int, error) {
	result, err := service.redis.LPop(activeKey).Result()
	if err != nil {
		return 0, err
	}

	err = service.redis.RPush(usedKey, result).Err()
	if err != nil {
		return 0, err
	}

	uniqueCode, err := strconv.Atoi(result)

	return uniqueCode, nil
}

// ReturnBack :nodoc
func (service *UniqueCodeService) ReturnBack() error {
	result, err := service.redis.LPop(usedKey).Result()
	if err != nil {
		return err
	}

	err = service.redis.RPush(activeKey, result).Err()
	if err != nil {
		return err
	}

	return nil
}
