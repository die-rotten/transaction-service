package services

import (
	"time"
	"transaction-service/dto"
	"transaction-service/models"
	"transaction-service/models/entity"
	"transaction-service/models/filters"
)

// TransactionServiceContract is contract of TransctionService
type TransactionServiceContract interface {
	Get(filter filters.TransactionFilter) ([]dto.Transaction, error)
	GetUnpaid(userID uint) ([]dto.Transaction, error)
	Find(id uint) (dto.Transaction, error)
	FindByUser(id, userID uint) (dto.Transaction, error)
	GetByUser(userID uint, filter filters.TransactionFilter) ([]dto.Transaction, error)
	Create(
		userID uint,
		packageID uint,
		amount float64,
		conductedAt time.Time,
		createdAt time.Time,
		updatedAt time.Time,
	) (models.Transaction, error)
}

// ConfirmationServiceContract is contract of ConfirmationService
type ConfirmationServiceContract interface {
	Find(id uint) (models.TransactionConfirmation, error)
	Approve(id uint, queueName string) (models.TransactionConfirmation, error)
	GetUnapprove() ([]models.TransactionConfirmation, error)
	Confirm(
		bankName,
		senderName,
		filePath string,
		amount float64,
		transactionID,
		userID uint,
		now time.Time,
	) (models.TransactionConfirmation, error)
}

// UniqueCodeServiceContract is contract of UniqueCodeService
type UniqueCodeServiceContract interface {
	Init(start, end uint)
	Generate() (int, error)
	ReturnBack() error
}

// ReportServiceContract is contract of ReportService
type ReportServiceContract interface {
	GetTransactionQuantity() (uint, error)
	GetTransactionAmountByYear(year uint) (entity.TransactionReportByYear, error)
}
