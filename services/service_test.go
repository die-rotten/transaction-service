package services

import (
	"testing"

	"github.com/joho/godotenv"
)

func TestMain(t *testing.M) {
	godotenv.Load("../.env.test")

	t.Run()
}
