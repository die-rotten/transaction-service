package shared

type key string

const (
	// UserID is key for auth request context
	UserID key = "userID"
)
