package main

import (
	"fmt"
	"os"
)

func getCommand() {
	args := os.Args[1:]

	if len(args) == 0 {
		fmt.Println("No command inserted...")
		return
	}

	switch args[0] {
	case "serve":
		serveHTTP()
	case "migrate":
		migrate()
	case "migrate:reset":
		reset()
	case "seed":
		seed()
	case "seed:unique-code":
		uniqueCodeService.Init(100, 250)
	default:
		fmt.Println("Command not available...")
	}
}
