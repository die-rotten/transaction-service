package mocks

import (
	"transaction-service/models"

	"github.com/stretchr/testify/mock"
)

// ConfirmationRepositoryMock :nodoc
type ConfirmationRepositoryMock struct {
	mock.Mock
}

// Find mocks find
func (m *ConfirmationRepositoryMock) Find(id uint) (models.TransactionConfirmation, error) {
	args := m.Called(id)
	return args.Get(0).(models.TransactionConfirmation), args.Error(1)
}

// Confirm mocks confirm
func (m *ConfirmationRepositoryMock) Confirm(data models.TransactionConfirmation) (models.TransactionConfirmation, error) {
	args := m.Called(data)
	return args.Get(0).(models.TransactionConfirmation), args.Error(1)
}

// GetUnapprove mocks getunapprove
func (m *ConfirmationRepositoryMock) GetUnapprove() ([]models.TransactionConfirmation, error) {
	args := m.Called()
	return args.Get(0).([]models.TransactionConfirmation), args.Error(1)
}
