package mocks

import "github.com/stretchr/testify/mock"

// UniqueCodeServiceMock :nodoc
type UniqueCodeServiceMock struct {
	mock.Mock
}

// Init mocks Init
func (m *UniqueCodeServiceMock) Init(start, end uint) {
	m.Called(start, end)
	return
}

// Generate mocks generate
func (m *UniqueCodeServiceMock) Generate() (int, error) {
	args := m.Called()
	return args.Int(0), args.Error(1)
}

// ReturnBack mocks ReturnBack
func (m *UniqueCodeServiceMock) ReturnBack() error {
	args := m.Called()
	return args.Error(0)
}
