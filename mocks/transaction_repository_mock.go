package mocks

import (
	"transaction-service/models"
	"transaction-service/models/filters"

	"github.com/stretchr/testify/mock"
)

// TransactionRepositoryMock mocks TransactionRepository
type TransactionRepositoryMock struct {
	mock.Mock
}

// Get mocks get
func (m *TransactionRepositoryMock) Get(filter filters.TransactionFilter) ([]models.Transaction, error) {
	args := m.Called(filter)
	return args.Get(0).([]models.Transaction), args.Error(1)
}

// Find mocks find
func (m *TransactionRepositoryMock) Find(id uint) (models.Transaction, error) {
	args := m.Called(id)
	return args.Get(0).(models.Transaction), args.Error(1)
}

// FindByUser mocks FindByUser
func (m *TransactionRepositoryMock) FindByUser(id, userID uint) (models.Transaction, error) {
	args := m.Called(id, userID)
	return args.Get(0).(models.Transaction), args.Error(1)
}

// GetBy mocks GetBy
func (m *TransactionRepositoryMock) GetBy(key string, value interface{}, filter filters.TransactionFilter) ([]models.Transaction, error) {
	args := m.Called(key, value, filter)
	return args.Get(0).([]models.Transaction), args.Error(1)
}

// Create mocks Create
func (m *TransactionRepositoryMock) Create(data models.Transaction) (models.Transaction, error) {
	args := m.Called(data)
	return args.Get(0).(models.Transaction), args.Error(1)
}

// GetUnpaid :nodoc
func (m *TransactionRepositoryMock) GetUnpaid(userID uint) ([]models.Transaction, error) {
	args := m.Called(userID)
	return args.Get(0).([]models.Transaction), args.Error(1)
}
