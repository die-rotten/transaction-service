package mocks

import "github.com/stretchr/testify/mock"

// ApprovalRepositoryMock :nodoc
type ApprovalRepositoryMock struct {
	mock.Mock
}

// Approve mocks approve
func (m *ApprovalRepositoryMock) Approve(confirmID uint) error {
	args := m.Called(confirmID)
	return args.Error(0)
}
