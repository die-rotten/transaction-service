package mocks

import (
	"transaction-service/models/entity"

	"github.com/stretchr/testify/mock"
)

// ReportRepositoryMock :nodoc:
type ReportRepositoryMock struct {
	mock.Mock
}

// GetTransactionsQuantity :nodocd
func (repo *ReportRepositoryMock) GetTransactionsQuantity() (uint, error) {
	args := repo.Called()
	parsedUint := uint(args.Int(0))
	return parsedUint, args.Error(1)
}

// GetTransactionAmountByYear :nodocd
func (repo *ReportRepositoryMock) GetTransactionAmountByYear(year uint) (entity.TransactionReportByYear, error) {
	args := repo.Called(year)
	return args.Get(0).(entity.TransactionReportByYear), args.Error(1)
}
