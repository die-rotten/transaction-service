package mocks

import (
	"transaction-service/dto"

	"github.com/stretchr/testify/mock"
)

// PackageItemRepositoryMock :nodoc
type PackageItemRepositoryMock struct {
	mock.Mock
}

// FetchItem mocks FetchItem
func (m *PackageItemRepositoryMock) FetchItem(id uint) (dto.Item, error) {
	args := m.Called(id)
	return args.Get(0).(dto.Item), args.Error(1)
}

// Fetch mocks Fetch
func (m *PackageItemRepositoryMock) Fetch(ids []uint) ([]dto.Item, error) {
	args := m.Called(ids)
	return args.Get(0).([]dto.Item), args.Error(1)
}
