package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"transaction-service/handlers"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func createRouter() chi.Router {
	router := chi.NewRouter()
	router.Use(middleware.Logger)

	router.Get("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := map[string]interface{}{
			"version":     "1.0.0",
			"name":        "Rotten/TransactionService",
			"description": "Microservice that responsible to handle all transaction related",
		}

		res, _ := json.Marshal(payload)
		w.Write(res)
	}))

	router.Mount("/transactions", handlers.NewTransactionHandler(transactionService).GetRoutes())
	router.Mount("/confirmations", handlers.NewConfirmationHandler(confirmationService, transactionService).GetRoutes())
	router.Mount("/reports", handlers.NewReportHandler(reportService).GetRoutes())

	return router
}

func serveHTTP() {
	router := createRouter()
	port := os.Getenv("APP_PORT")
	if port == "" {
		port = "8000"
	}

	fmt.Printf("Server is start at port %s\n", port)
	http.ListenAndServe(fmt.Sprintf(":%s", port), router)
}
