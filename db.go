package main

import (
	"fmt"
	"os"
	"sync"
	"transaction-service/models"

	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // PostgreSQL dialect.
)

var (
	db          *gorm.DB
	redisClient *redis.Client
	once        sync.Once
)

func initDB() {
	once.Do(func() {
		connString := fmt.Sprintf(
			"host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
			os.Getenv("DB_HOST"),
			os.Getenv("DB_PORT"),
			os.Getenv("DB_USERNAME"),
			os.Getenv("DB_DATABASE"),
			os.Getenv("DB_PASSWORD"),
		)

		conn, err := gorm.Open("postgres", connString)
		if err != nil {
			panic(err)
		}

		db = conn
	})
}

func migrate() {
	db.AutoMigrate(
		&models.Transaction{},
		&models.TransactionItem{},
		&models.TransactionConfirmation{},
		&models.Status{},
	)

	fmt.Println("Migration completed!")
}

func reset() {
	db.DropTable(
		&models.Transaction{},
		&models.TransactionItem{},
		&models.TransactionConfirmation{},
		&models.Status{},
	)

	migrate()

	fmt.Println("Reset database completed!")
}

func seed() {
	seedStatus(db)
}

func initRedis() {
	address := fmt.Sprintf(
		"%s:%s",
		os.Getenv("REDIS_HOST"),
		os.Getenv("REDIS_PORT"),
	)

	if redisClient == nil {
		env := os.Getenv("APP_ENVIRONMENT")

		var password string
		if env == "production" {
			password = os.Getenv("REDIS_PASSWORD")
		} else {
			password = ""
		}

		redisClient = redis.NewClient(&redis.Options{
			Addr:     address,
			Password: password,
			DB:       0,
		})
	}
}
