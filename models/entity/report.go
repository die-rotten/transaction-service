package entity

// TransactionMonthlyReport :nodoc
type TransactionMonthlyReport struct {
	Month  string  `json:"month"`
	Amount float64 `json:"amount"`
}

// TransactionReportByYear :nodoc
type TransactionReportByYear struct {
	Year uint                       `json:"year"`
	Data []TransactionMonthlyReport `json:"data"`
}

// MonthList :nodoc
var MonthList = []map[string]interface{}{
	{
		"key":  1,
		"name": "January",
	},
	{
		"key":  2,
		"name": "February",
	},
	{
		"key":  3,
		"name": "March",
	},
	{
		"key":  4,
		"name": "April",
	},
	{
		"key":  5,
		"name": "May",
	},
	{
		"key":  6,
		"name": "June",
	},
	{
		"key":  7,
		"name": "July",
	},
	{
		"key":  8,
		"name": "August",
	},
	{
		"key":  9,
		"name": "September",
	},
	{
		"key":  10,
		"name": "October",
	},
	{
		"key":  11,
		"name": "November",
	},
	{
		"key":  12,
		"name": "December",
	},
}
