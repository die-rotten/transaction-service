package models

import "time"

// Transaction is representative of transaction model.
type Transaction struct {
	ID            uint
	Number        string
	UserID        uint
	StatusID      uint
	UniqueCode    uint
	ConductedAt   time.Time
	IsPaid        bool
	Amount        float64
	Item          TransactionItem
	Status        Status
	Confirmations []TransactionConfirmation
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     *time.Time
}

// TransactionItem is representative of transaction item model
type TransactionItem struct {
	ID            uint
	TransactionID uint
	PackageID     uint
	Quantity      uint
	Amount        float64
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     *time.Time
}

// TransactionConfirmation is representative of transaction confirmation model
type TransactionConfirmation struct {
	ID            uint
	TransactionID uint
	UserID        uint
	IsApproved    bool
	BankName      string
	SenderName    string
	Amount        float64
	FilePath      string
	Transaction   Transaction
	ConductedAt   time.Time
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     *time.Time
}
