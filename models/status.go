package models

// Status :nodoc
type Status struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}
